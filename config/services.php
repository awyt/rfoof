<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Vendor::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],	
		
	'facebook' => [
		'client_id' => '406021406722965',
		'client_secret' => 'd61bc60ad039d4a0d9792ec7cfb50fc6',
		'redirect' => 'https://rfoof.com/login/facebook/callback',
	],
	
	
	'google' => [
		'client_id' => '858442814109-t5fp8ut41mslkh3m8ck8p1h5jh99c9jj.apps.googleusercontent.com',
		'client_secret' => 'ts-yJL8XSBQGXJMlsHS3pnJU',
		'redirect' => 'http://rfoof.com/login/google/callback',
	],

];
