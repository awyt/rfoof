<?php
/*
Project Name: IonicEcommerce
Project URI: http://ionicecommerce.com
Author: VectorCoder Team
Author URI: http://vectorcoder.com/
*/
namespace App\Http\Controllers\App;

//validator is builtin class in laravel
use Validator;

use DB;
use DateTime;
//for password encryption or hash protected
use Hash;

//for authenitcate login data
use Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;

//for requesting a value 
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//for Carbon a value 
use Carbon;

class VendorsController extends Controller
{
	
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	
	
	//getallproducts 
	public function getallvendors(Request $request){
		$language_id            				=   $request->language_id;	
		$skip									=   $request->page_number.'0';
		$currentDate 							=   time();	
		$type									=	$request->type;
		
		//filter
		$minPrice	 							=   $request->price['minPrice'];
		$maxPrice	 							=   $request->price['maxPrice'];
		$consumer_data 		 				 	=  array();
		$consumer_data['consumer_key'] 	 	    =  request()->header('consumer-key');
		$consumer_data['consumer_secret']	    =  request()->header('consumer-secret');
		$consumer_data['consumer_nonce']	    =  request()->header('consumer-nonce');	
		$consumer_data['consumer_device_id']    =  request()->header('consumer-device-id');	
		$consumer_data['consumer_url']  	    =  __FUNCTION__;
		
		$authController = new AppSettingController();
		$authenticate = $authController->apiAuthenticate($consumer_data);
		if($authenticate==1){	
		
		if($type=="a to z"){
			$sortby								=	"vendor_id";
			$order								=	"ASC";
		}elseif($type=="z to a"){
			$sortby								=	"vendor_id";
			$order								=	"DESC";
		
		}else{
			$sortby = "vendors.vendor_id";
			$order = "desc";
		}
		
		
		$filtervendors = array();
		$eliminateRecord = array();
		if(!empty($request->filters)){
		
		}else{		
			$vendors = DB::table('vendors')				
				->where('isActive',1);
				
				if(!empty($request->vendor_id)&& $request->vendor_id!=""){
				
					$vendors->where('vendor_id',$request->vendor_id);
				}
				if(!empty($request->city_id)&& $request->city_id!=""){
				
					$vendors->where('city_id',$request->city_id);
				}
				if(!empty($request->cat_id)&& $request->cat_id!=""){
				
					$vendors->leftJoin('vendors_categories','vendors_categories.vendor_id','vendors.vendor_id')->where('vendors_categories.category_id',$request->cat_id);
				}
					
			
				$vendors->select('vendors.vendor_id','vendors.company_name as vendor_name','vendors.vendor_name as Employee','vendors.company_log as vendor_logo','vendors.entity_registration_number','vendors.commercial_number','vendors.email','vendors.vendor_address','vendors.vendor_phone','vendors.vendor_fax','vendors.vendor_code','vendors.directions', 'vendors.description')
                                        ->orderBy($sortby, $order);
			
			
			//count
			$total_record = $vendors->get();
						
			$data  = $vendors->skip($skip)->take(10)->get();
			$cities = DB::table('cities')->get();
			$result = array();
			$result2 = array();
                        if(count($cities)>0){
                            foreach ($cities as $city){
				$city->city_id = $city->id;
				$city->city_name = $city->city_name;
				array_push($result2,$city);
                            }
                            }
			//check if record exist
			if(count($data)>0){
                            foreach ($data as $vendor){
				
				
				array_push($result,$vendor);
                            }
                            
                                $responseData = array('success'=>'1', 'vendors_data'=>$result, 'cities'=>$result2,  'message'=>"Returned all Sellers.", 'total_record'=>count($total_record));
				}else{
					$responseData = array('success'=>'0', 'vendors_data'=>$result, 'cities'=>$result2,  'message'=>"Empty record.", 'total_record'=>count($total_record));
				}		
		}
		}else{
			$responseData = array('success'=>'0', 'vendors_data'=>array(),  'message'=>"Unauthenticated call.");
		}
		$vendorResponse = json_encode($responseData);
		print $vendorResponse;
	}	
	
	
	
}
