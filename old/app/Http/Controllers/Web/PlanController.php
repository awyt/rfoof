<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Plan;
use Illuminate\Support\Facades\Lang;


class PlanController extends DataController
{
  
public function index()
{
        $plans = Plan::all();
        $pageTitle = array('pageTitle' => Lang::get("website.Sign Up"));
			$result = array();
		$result['commonContent'] = $this->commonContent();
//        return view('plans', compact('plans','pageTitle'));
        return view("plans", $pageTitle)->with(['result'=>$result,'plans'=>$plans]); 
}

public function show(Plan $plan, Request $request)
{
     return view('plansshow', compact('plan'));
}
}
