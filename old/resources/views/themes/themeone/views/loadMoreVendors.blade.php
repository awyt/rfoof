@extends('layout')
@section('content')

<section class="site-content">
    <div class="container vendors-page">
    <div class="breadcum-area">
        <div class="breadcum-inner">
            <h3>@lang('website.Vendors')</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL::to('/')}}">@lang('website.Home')</a></li>
                @if(!empty($result['category_name']))
                <li class="breadcrumb-item"><a href="{{ URL::to('/vendors')}}">@lang('website.Vendors')</a></li>
                <li class="breadcrumb-item active">{{$result['category_name']}}</li>
                @else
                <li class="breadcrumb-item active">@lang('website.Vendors')</li>
                @endif
            </ol>
        </div>
    </div>

    <!--filters-->
        <div class=" ">
            <div class="row vendors-filters">
                <div class="col-12">

                    <form method="get" id="load_vendors_form" action="{{url('vendors')}}">        
                        <div class="form-row">

                            <div class=" col-md-6">
                                <div class="input-group ">
                                    <input type="text" name="searchword" @if(!empty($result['searchword']))value="{{$result['searchword']}}" @endif class="form-control" placeholder="@lang('website.Search')" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                           <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">@lang('website.Search')</span>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-md-3">
                                <!--<label for="cities">cities</label>-->
                                <select id="cities-select" class="form-control" name='city_id'>
                                    <option  value="0">select city</option>
                                    @foreach($result['cities'] as $city)
                                    @if($result['city_id']==$city->id)
                                    <option selected value="{{$city->id}}">{{$city->city_name}}</option>
                                    @else
                                    <option value="{{$city->id}}">{{$city->city_name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class=" col-md-3">
                                <!--<label for="categories">categories</label>-->
                                <select id="categories-select" class="form-control" name='cat_id'>
                                    <option value="0">select category</option>
                                    @foreach($result['categories'] as $category)
                                    @if($result['cat_id']== $category->categories_id)
                                    <option selected value="{{$category->categories_id}}">{{$category->categories_name}}</option>
                                    @else
                                    <option value="{{$category->categories_id}}">{{$category->categories_name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    

                </div>

            </div>
        </div>

        <!--filters-->
    
    
        <!--vendors-list-->

@if($result['vendors']['success']==1)

        <div class="vendors-list">
            <div class="row ">
                <div class="col-6 col-md-6 col-lg-12 " id="listing-vendors">


@foreach($result['vendors']['vendors'] as $vendor)

    <div class="vendor">
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-2 ">
                                <div class="vendor-logo">
                                    <!-- default image -->
                                    <div class="">
                                        @if($vendor->is_portal==1)
                                        <img class="img-fluid" src="http://seller.rfoof.com/resources/assets/images/vendors/{{$vendor->company_log}}" >
                                        @else
                                        <img class="img-fluid" src="{{asset($vendor->company_log) }}" alt="vendor-logo">
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-12 col-lg-10">


                                <div class="vendor-data">

                                    <h3 class="vendor-data-title">{{$vendor->company_name}}</h3>
                                    <hr>
                                    <div class="vendor-info">
                                        <div class="vendor-info-statistic">
                                            <?php
                                            $sales = DB::table('orders')->where('vendor_id', $vendor->vendor_id)
                                                            ->LeftJoin('orders_status_history', 'orders_status_history.orders_id', '=', 'orders.orders_id')
                                                            ->where('orders_status_history.orders_status_id', 2)->get();
                                            $products = DB::table('products')->where('vendor_id', $vendor->vendor_id)->get();
                                            ?>
                                            <span href="#" >@lang('website.nosales') <span class="vendor-info-statistic-num">({{count($sales)}})</span></span>
                                            <!--<span href="#">seller rate<span class="vendor-info-statistic-num">(2)</span></span>-->
                                            <span href="#">@lang('website.noproducts') <span class="vendor-info-statistic-num">({{count($products)}})</span></span>

                                        </div>
                                        <ul class="vendor-data-contact-list">
                                            <li>
                                                <span> <i class="fa fa-map-marker" aria-hidden="true"></i>  {{$vendor->vendor_address}}</span>
                                                @if(!empty($vendor->directions))
                                                <a href="{{$vendor->directions}}"<span>get direction <i class="fa fa-map-signs" aria-hidden="true"></i></span>
                                                    @endif
                                            </li>

                                            <li>
                                                <span> <i class="fa fa-phone" aria-hidden="true"></i> @lang('website.company phone') : {{$vendor->vendor_phone}}</span>
                                                <span><i class="fa fa-fax" aria-hidden="true"></i> @lang('website.company fax') : {{$vendor->vendor_fax}}</span>
                                                <a href="{{url('/vendorprofile/'.$vendor->vendor_id)}}"> <button class="vendor-details-btn btn float-right">details</button></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

@endforeach
@if(count($result['vendors']['vendors'])> 0 and $result['limit'] > count($result['vendors']['vendors']))
     <style>
        #load_vendors{
            display: none;
        }
        #loaded_content{
            display: block !important;
        }
        #loaded_content_empty{
            display: none !important;
        }
     </style>
@endif

@elseif(count($result['vendors']['vendors'])== 0 or $result['vendors']['success']==0 or count($result['vendors']['vendors']) < $result['limit'])
    <style>
        #load_vendors{
            display: none;
        }
        #loaded_content{
            display: none !important;
        }
        #loaded_content_empty{
            display: block !important;
        }
    </style>
@endif
    </div>

</div>
<!--vendors-list-->

</div>
</section

@endsection