@extends('layout')
@section('content')

<section class="site-content">
	<div class="container">
    	<div class="breadcum-area">
            <div class="breadcum-inner">
                <h3>@lang('website.categories')</h3>
                <ol class="breadcrumb">                    
                    <li class="breadcrumb-item"><a href="{{ URL::to('/')}}">@lang('website.Home')</a></li>
                    <li class="breadcrumb-item"><a href="{{ URL::to('/categories')}}">@lang('website.categories')</a></li>
                </ol>
            </div>
        </div>
		<div class="shop-area">
<div class="container-fuild">
  <div class="container">
    <div class="products-area"> 
      <!-- heading -->
      <div class="heading">
        <h2>@lang('website.Categories') <small class="pull-right"><!--<a href="shop" >@lang('website.View All')</a>--></small></h2>
        <hr>
      </div>
        <div class="row"> 
            <div class="col-xs-12 col-sm-12">
                <div class="row">
                    <!-- Items -->
                    <div class="products products-5x">
                        <!-- categories --> 
                        @foreach($result['commonContent']['categories'] as $categories_data)
                                <div class="product">
                                    <div class="blog-post">
                                        <article>
                                            <div class="module">
                                            	<a href="{{ URL::to('/shop?category='.$categories_data->slug)}}" class="cat-thumb">
                                                   <img class="img-fluid" src="{{asset('').$categories_data->image}}" alt="{{$categories_data->name}}">             
                                                </a>
                                                <a href="{{ URL::to('/shop?category='.$categories_data->slug)}}" class="cat-title">
                                                	{{$categories_data->name}}
                                                </a>
                                            </div>
                                        </article>
                                    </div>
                                </div>

                        @endforeach	
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

		</div>
	</div>
</section>
@endsection 