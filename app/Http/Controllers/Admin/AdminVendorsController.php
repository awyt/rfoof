<?php
/*
Project Name: IonicEcommerce
Project URI: http://ionicecommerce.com
Author: VectorCoder Team
Author URI: http://vectorcoder.com/

*/
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;


use Validator;
use App;
use Lang;
use DB;
use Excel;
//for password encryption or hash protected

use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;
//for redirect
use Illuminate\Support\Facades\Redirect;


//for requesting a value 
use Illuminate\Http\Request;

class AdminVendorsController extends Controller
{	
	//add listingvendors
	public function vendors(Request $request){
//		if(session('vendors_view')==0){
//			print Lang::get("labels.You do not have to access this route");
//		}else{
			
		$title = array('pageTitle' => Lang::get("labels.Listingvendors"));
		$language_id            				=   '1';			
		
		$vendorData = array();
		$message = array();
		$errorMessage = array();
		
		$data = DB::table('vendors');
			
                if(isset($_REQUEST['details']) and !empty($_REQUEST['details'])){
                            $prod=$_REQUEST['details'];
				$data->where(function ($query) use ($prod) {
                                   $query-> where('qid_number', 'like', '%' . $_REQUEST['details'] . '%')
                                        ->orWhere('entity_registration_number', 'like', '%' . $_REQUEST['details'] . '%')
                                        ->orWhere('vendor_name', 'like', '%' . $_REQUEST['details'] . '%')
                                        ->orWhere('commercial_number', 'like', '%' . $_REQUEST['details'] . '%')
                                        ->orWhere('company_name', 'like', '%' . $_REQUEST['details'] . '%')
                                        ->orWhere('vendor_code', 'like', '%' . $_REQUEST['details'] . '%');
			});
                        }
        if (isset($_REQUEST['status']) and ! empty($_REQUEST['status'])) {
            if ($_REQUEST['status'] == 'verify') {
                $data->where('is_verified', '=', 1);
            } elseif ($_REQUEST['status'] == 'active') {
                $data->where('isActive', '=', 1);
            } elseif ($_REQUEST['status'] == 'review') {
                $data->where('is_reviewed', '=', 1);
            }
        }


        $vendors = $data->orderBy('vendors.vendor_id','ASC')
                                    ->paginate(40);
	
		$result = array();
		$index = 0;
		foreach($vendors as $vendors_data){
			array_push($result, $vendors_data);
			
			$files = DB::table('vendor_files')->where('vendor_id','=',$vendors_data->vendor_id)->get();
			$result[$index]->files = $files;
			$index++;
		}
		
		$vendorData['message'] = $message;
		$vendorData['errorMessage'] = $errorMessage;
		$vendorData['result'] = $vendors;
		$vendorData['statuses'] = array("verify"=>"Is Verified", "review"=>"Is Approved", "active"=>"Is Active");
		return view("admin.vendors",$title)->with('vendors', $vendorData);
//                }
	}
        
        public function getVendor(Request $request){


		$vendors = DB::table('vendors')
                        ->select('vendors.*')
                        -> where('company_name', 'like', '%' . $request->vendor_id . '%')
			->orWhere('vendor_code','like', '%' . $request->vendor_id . '%')
			->get();

			

		if(count($vendors)>0){	

			foreach($vendors as $value_data){

				$value_name[] = "<option value='".$value_data->vendor_id."'>".$value_data->vendor_code."-".$value_data->company_name."</option>";	

			}

		}else{

			$value_name = "<option value=''>".Lang::get("labels.ChooseValue")."</option>";

		}

		print_r($value_name);

	}

	
	//add addvendors page
	public function addvendors(Request $request){
//		if(session('vendors_view')==0){
//			print Lang::get("labels.You do not have to access this route");
//		}else{
		$title = array('pageTitle' => Lang::get("labels.AddNewVendors"));
		$language_id            				=   '1';	
		
		$vendorData = array();
		$message = array();
		$errorMessage = array();
		//get function from other controller
		$myVar = new AdminCategoriesController();
		$vendorData['categories'] = $myVar->allCategories($language_id);
		$cities = DB::table('cities')->get();
		$vendorData['cities'] =$cities;
		$vendorData['message'] = $message;
		$vendorData['errorMessage'] = $errorMessage;
		
		return view("admin.addvendors",$title)->with('vendors', $vendorData);
//		}
	}
	
	//add addvendors data and redirect to address
	public function addnewvendors(Request $request){
            
// 		if(session('vendors_create')==0){
// 			print Lang::get("labels.You do not have to access this route");
// 		}else{		
		$language_id            				=   '1';
		
		//get function from other controller
		$myVar = new AdminSiteSettingController();	
		$extensions = $myVar->imageType();			
		
		$vendorData = array();
		$message = array();
		$errorMessage = array();
		
		//check email already exists
		$existEmail = DB::table('vendors')->where('email', '=', $request->email)->get();
		if(count($existEmail)>0){
			$title = array('pageTitle' => 'Add vendor');
			
			$vendorData['message'] = $message;
			$vendorData['errorMessage'] = Lang::get("labels.Email address already exist");
			$cities = DB::table('cities')->get();
		$vendorData['cities'] =$cities;
			return view("admin.addvendors",$title)->with('vendors', $vendorData);
		}else{
						
			if($request->hasFile('company_log') and in_array($request->company_log->extension(), $extensions)){
				$image = $request->company_log;
				$fileName = time().'.'.$image->getClientOriginalName();
				$image->move('resources/assets/images/vendors/', $fileName);
				$vendors_picture = 'resources/assets/images/vendors/'.$fileName; 
			}	else{
				$vendors_picture = '';
			}			
			
			$vendors_id = DB::table('vendors')->insertGetId([
						'company_name'   		 	=>   $request->company_name,
						'vendor_name'		 	=>   $request->vendor_name,
						'entity_registration_number'		 	=>   $request->entity_registration_number,
						'commercial_number'	 			 	=>	 $request->commercial_number,
						'qid_number'   		 	=>   $request->qid_number,
						'company_log'   		 	=>   $vendors_picture,
						'email'	 	=>   $request->email,
						'vendor_address' 	=>   $request->vendor_address,
						'vendor_phone'	 		=>	 $request->vendor_phone,
						'vendor_fax'   				=>   $request->vendor_fax,
						'password'		 				=>   Hash::make($request->password),
						'vendor_code'   		 	=>   $request->vendor_code,
						'city_id'   		 	=>   $request->city_id,
						'directions'   		 	=>   $request->directions,
						'description'   		 	=>   $request->description,
						'isActive'		 	 			=>   $request->isActive,
						'is_reviewed'		 	 			=>   1,
						'created_at'					 =>	 time()
						]);
                        if($request->hasFile('contract')){
				$image = $request->contract;
				$fileName = time().'.'.$image->getClientOriginalName();
				$image->move('resources/assets/contracts/vendors/', $fileName);
				$vendor_contract = 'resources/assets/contracts/vendors/'.$fileName; 
                                $files_id = DB::table('vendor_files')->insertGetId([
						'vendor_id'   		 	=>   $vendors_id,
						'file_path'		 	=>   $vendor_contract,
						'type'		 	=>   'contract',
						'is_default'		=>   1,
						'created_at'					 =>	 time()
						]);
			}
                        if($request->has('vendor_cats')){
                            foreach($request->vendor_cats as $category){
			DB::table('vendors_categories')->insert([
						'vendor_id'   	=>     $vendors_id,
						'category_id'     =>     $category
					]);
		}
                        }
					
			return redirect('admin/vendors')->withErrors([Lang::get("labels.AddeddvendorMessage")]);		
		}
// 		}
	}
	
	
	
	//editvendors data and redirect to address
	public function editvendors($id){
//		if(session('vendors_view')==0){
//			print Lang::get("labels.You do not have to access this route");
//		}else{
		$title = array('pageTitle' => Lang::get("labels.Editvendor"));
		$language_id             =   '1';	
		$vendors_id        	 =   $id;			
		
		$vendorData = array();
		$message = array();
		$errorMessage = array();
		//get function from other controller
		$myVar = new AdminCategoriesController();
		$vendorData['categories'] = $myVar->allCategories($language_id);
		$cities = DB::table('cities')->get();
		$vendorData['cities'] =$cities;
		$vendors = DB::table('vendors')->where('vendor_id','=', $vendors_id)->first();
		$vendor_cats = DB::table('vendors_categories')->where('vendor_id','=', $vendors_id)->get();
                $vcats=array();
		foreach ($vendor_cats as $cat){
                    array_push($vcats, $cat->category_id);
                }
		$vendorData['message'] = $message;
		$vendorData['errorMessage'] = $errorMessage;
		$vendorData['vendor'] = $vendors;
		$vendorData['vendor_cats'] = $vcats;
		return view("admin.editvendors",$title)->with('vendors', $vendorData);
//		}
	}
		
	//update addvendors data 
	public function updatevendors(Request $request){
            
//		if(session('vendors_update')==0){
//			print Lang::get("labels.You do not have to access this route");
//		}else{		
		$language_id            		=   '1';			
		$vendors_id					=	$request->vendor_id;
		
		$vendorData = array();
		$message = array();
		$errorMessage = array();
		
		//get function from other controller
		$myVar = new AdminSiteSettingController();	
		$extensions = $myVar->imageType();	
				
		if($request->hasFile('company_log') and in_array($request->company_log->extension(), $extensions)){
			$image = $request->company_log;
			$fileName = time().'.'.$image->getClientOriginalName();
			$image->move('resources/assets/images/vendors/', $fileName);
			$vendors_picture = 'resources/assets/images/vendors/'.$fileName; 
		}	else{
			$vendors_picture = $request->oldImage;
		}		
		
		$vendor_data = array(
			'company_name'   		 	=>   $request->company_name,
						'vendor_name'		 	=>   $request->vendor_name,
						'entity_registration_number'		 	=>   $request->entity_registration_number,
						'commercial_number'	 			 	=>	 $request->commercial_number,
						'qid_number'   		 	=>   $request->qid_number,
						'company_log'   		 	=>   $vendors_picture,
						'vendor_address' 	=>   $request->vendor_address,
						'vendor_phone'	 		=>	 $request->vendor_phone,
						'vendor_fax'   				=>   $request->vendor_fax,
						'password'		 				=>   Hash::make($request->password),
						'vendor_code'   		 	=>   $request->vendor_code,
						'city_id'   		 	=>   $request->city_id,
						'directions'   		 	=>   $request->directions,
						'description'   		 	=>   $request->description,
						'isActive'		 	 			=>   $request->isActive,
						'updated_at'					 =>	 time()
		);
		
		if($request->changePassword == 'yes'){
			$vendor_data['password'] = Hash::make($request->password);
		}
		
		//check email already exists
		if($request->old_email_address!=$request->email){
			$existEmail = DB::table('vendors')->where('email', '=', $request->email)->get();
			if(count($existEmail)>0){
				$title = array('pageTitle' => Lang::get("labels.Editvendor"));
				
				$vendorData['message'] = $message;
				$vendorData['errorMessage'] = 'Email address already exist.';
				return view("admin.editvendors",$title)->with('vendors', $vendorData);
			}else{
				DB::table('vendors')->where('vendor_id', '=', $vendors_id)->update($vendor_data);					 
						
			}
		}else{
			DB::table('vendors')->where('vendor_id', '=', $vendors_id)->update($vendor_data);					 
		}
                if($request->hasFile('contract')){
                    		DB::table('vendor_files')->where('vendor_id','=', $vendors_id)->where('type','contract')->delete();
				$image = $request->contract;
				$fileName = time().'.'.$image->getClientOriginalName();
				$image->move('resources/assets/contracts/vendors/', $fileName);
				$vendor_contract = 'resources/assets/contracts/vendors/'.$fileName; 
                                $files_id = DB::table('vendor_files')->insertGetId([
						'vendor_id'   		 	=>   $vendors_id,
						'file_path'		 	=>   $vendor_contract,
						'type'		 	=>   'contract',
						'is_default'		=>   1,
						'updated_at'					 =>	 time(),
						'created_at'					 =>	 time()
						]);
			}
                        if($request->has('vendor_cats')){
                            DB::table('vendors_categories')->where('vendor_id','=', $vendors_id)->delete();
                            foreach($request->vendor_cats as $category){
                                        DB::table('vendors_categories')->insert([
						'vendor_id'   	=>     $vendors_id,
						'category_id'     =>     $category
					]);
                            }
                        }else{
                            DB::table('vendors_categories')->where('vendor_id','=', $vendors_id)->delete();

                        }
                        return redirect('admin/vendors')->withErrors([Lang::get("labels.UpdatedvendorMessage")]);
//		}
	}
	
	
	//deletevendor
	public function deletevendors(Request $request){
//		if(session('vendors_delete')==0){
//			print Lang::get("labels.You do not have to access this route");
//		}else{
			
		$vendors_id = $request->vendors_id;
		DB::table('vendors')->where('vendor_id','=', $vendors_id)->delete();
		DB::table('vendor_files')->where('vendor_id','=', $vendors_id)->delete();
		DB::table('vendors_categories')->where('vendor_id','=', $vendors_id)->delete();
		
		return redirect()->back()->withErrors([Lang::get("labels.DeletevendorMessage")]);
//		}
	}
	public function approvevendors(Request $request){
//		if(session('vendors_delete')==0){
//			print Lang::get("labels.You do not have to access this route");
//		}else{
			
		$vendors_id = $request->vendors_id;
		$vendor=DB::table('vendors')->where('vendor_id','=', $vendors_id)->update(['is_reviewed'=>1,'isActive'=>1]);
		
		return redirect()->back()->withErrors([Lang::get("labels.ApprovedvendorMessage")]);
//		}
	}
	public function changeStatus(Request $request){
//		if(session('vendors_delete')==0){
//			print Lang::get("labels.You do not have to access this route");
//		}else{
			
		$vendors_id = $request->vendors_id;
		$vendor=DB::table('vendors')->where('vendor_id','=', $vendors_id)->first();
		$status=$vendor->isActive;
                DB::table('vendors')->where('vendor_id','=', $vendors_id)->update(['isActive'=>1-$status]);
                
		return redirect()->back()->withErrors([Lang::get("labels.ApprovedvendorMessage")]);
//		}
	}
	public function downloadExcel()
	{
		$vendors = DB::table('vendors')->orderBy('created_at','DESC')
			->select('vendor_id','vendor_code','company_name','vendor_name','qid_number','entity_registration_number','commercial_number','vendor_address','vendor_phone','vendor_fax','is_reviewed','is_verified','isActive')->get();
		
		$status='';
                
		foreach($vendors as $vendor){
                    if($vendor->isActive==1){
                        $status="Active";
                    }elseif($vendor->is_reviewed==1){
                        $status="Approved";
                    }else{
                $status="Waiting Approve";
            }
                 $data[] = array(
                    $vendor->vendor_id,
                    $vendor->vendor_code,
                    $vendor->company_name,
                    $vendor->vendor_name,
                    $vendor->qid_number,
                    $vendor->entity_registration_number,
                    $vendor->commercial_number,
                    $vendor->vendor_address,
                    $vendor->vendor_phone,
                    $vendor->vendor_fax,
                    $status
                );
                }
		return Excel::create('Sellers', function($excel) use ($data) {
			$excel->sheet('Sellers', function($sheet) use ($data)
	        {
				$sheet->fromArray($data, null, 'A1', false, false);
                                $headings = array('ID', 'Code', 'Name', 'Employee', 'QID','Register num','Commercial num','Address','Phone','Fax','Status');
                                $sheet->prependRow(1, $headings);
	        });
		})->download('xls');
	}
	
	
}
