@extends('admin.layout')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> {{ trans('labels.Vendors') }} <small>{{ trans('labels.AddNewVendors') }}...</small> </h1>
    <ol class="breadcrumb">
      <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
      <li><a href="{{ URL::to('admin/vendors')}}"><i class="fa fa-users"></i> {{ trans('labels.ListingAllVendors') }}</a></li>
      <li class="active">{{ trans('labels.AddNewVendors') }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->

    <!-- /.row -->

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ trans('labels.AddNewVendors') }} </h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
              		<div class="box box-info">
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit category</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <br>
                       	@if(!empty($vendors['message']))
						<div class="alert alert-success alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ $vendors['message'] }}
						</div>
						@endif

                       @if(!empty($vendors['errorMessage']))
						<div class="alert alert-danger" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ $vendors['errorMessage'] }}
						</div>
						@endif

                        <!-- form start -->
                         <div class="box-body">
                            {!! Form::open(array('url' =>'admin/addnewvendors', 'method'=>'post', 'class' => 'form-horizontal form-validate','id' => 'vendorForm', 'enctype'=>'multipart/form-data')) !!}
                            
                            <div class="form-group">
                                            <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.CityName') }}
                                            </label>
                                            <div class="col-sm-10 col-md-4">
                                                <select name="city_id" class="form-control">
                                                    @foreach($vendors['cities'] as $country)
                                                    <option value="{{ $country->id }}"> {{ $country->city_name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.CityNameText') }}</span>
                                            </div>
                                        </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Category') }}</label>
                                <div class="col-sm-10 col-md-4">
                                    <select name="vendor_cats[]" class="form-control select2 field-validate" multiple="multiple" data-placeholder="{{ trans('labels.ChooseCatgoryText') }}"
                                            style="width: 100%;">
                                        @foreach($vendors['categories'] as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ChooseCatgoryText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>

                                </div>
                            </div>
                            

                                <div class="form-group">
                                  <label for="company_name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.company name') }} </label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('company_name',  '', array('class'=>'form-control field-validate', 'id'=>'company_name')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.companynameText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="vendor_name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.employee name') }} </label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('vendor_name',  '', array('class'=>'form-control field-validate', 'id'=>'vendor_name')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.employeenameText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="entity_registration_number" class="col-sm-2 col-md-3 control-label">{{ trans('labels.registration_number') }} </label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('entity_registration_number',  '', array('class'=>'form-control field-validate', 'id'=>'vendor_name')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.registration_numberText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="commercial_number" class="col-sm-2 col-md-3 control-label">{{ trans('labels.commercial_number') }} </label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('commercial_number',  '', array('class'=>'form-control field-validate', 'id'=>'commercial_number')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.commercial_numberText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="qid_number" class="col-sm-2 col-md-3 control-label">{{ trans('labels.qid_number') }} </label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('qid_number',  '', array('class'=>'form-control field-validate', 'id'=>'commercial_number')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.qid_numberText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="vendor_code" class="col-sm-2 col-md-3 control-label">{{ trans('labels.code') }} </label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('vendor_code',  '', array('class'=>'form-control' , 'id'=>'vendor_code')) !!}
                                 	 <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                 	 {{ trans('labels.codeText') }}</span>
                                    
                                  </div>
                                </div>
                            
                                <div class="form-group">
                                  <label for="vendor_address" class="col-sm-2 col-md-3 control-label">{{ trans('labels.company address') }} </label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('vendor_address',  '', array('class'=>'form-control field-validate' , 'id'=>'vendor_address')) !!}
                                 	 <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                 	 {{ trans('labels.company addressText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="directions" class="col-sm-2 col-md-3 control-label">{{ trans('labels.directions') }} </label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('directions',  '', array('class'=>'form-control' , 'id'=>'directions')) !!}
                                 	 <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                 	 {{ trans('labels.directionsText') }}</span>
                                    
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="vendor_phone" class="col-sm-2 col-md-3 control-label">{{ trans('labels.company phone') }}</label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('vendor_phone',  '', array('class'=>'form-control field-validate', 'id'=>'vendor_phone')) !!}
                                   <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                   {{ trans('labels.company phoneText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="vendor_fax" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Fax') }}</label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('vendor_fax',  '', array('class'=>'form-control', 'id'=>'vendor_fax')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FaxText') }}</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="description" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Description') }}</label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::textarea('description',  '', array('class'=>'form-control', 'id'=>'description')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.DescriptionText') }}</span>
                                  </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                  <label for="company_log" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Upload company logo') }} </label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::file('company_log', array('id'=>'newImage','class'=>'field-validate')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                    {{ trans('labels.Upload company logoText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    <br>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="contract" class="col-sm-2 col-md-3 control-label">{{ trans('labels.company upload contract') }} </label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::file('contract', array('id'=>'contract')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                    {{ trans('labels.company upload contractText') }}</span>
                                    <br>
                                  </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.EmailAddress') }} </label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('email',  '', array('class'=>'form-control email-validate', 'id'=>'email')) !!}
                                     <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                     {{ trans('labels.EmailText') }}</span>
                                    <span class="help-block hidden"> {{ trans('labels.EmailError') }}</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Password') }}</label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::password('password', array('class'=>'form-control field-validate', 'id'=>'password')) !!}
                	                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                   {{ trans('labels.PasswordText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Status') }} </label>
                                  <div class="col-sm-10 col-md-4">
                                    <select class="form-control" name="isActive">
                                          <option value="1">{{ trans('labels.Active') }}</option>
                                          <option value="0">{{ trans('labels.Inactive') }}</option>
									</select>
                                  <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                  {{ trans('labels.StatusText') }}</span>
                                  </div>
                                </div>

                              <!-- /.box-body -->
                              <div class="box-footer text-center">
                                <button type="submit" onClick='submitVendorForm()' class="btn btn-primary">{{ trans('labels.Add') }}</button>
                                <a href="{{ URL::to('admin/vendors')}}" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
                              </div>
                              <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>
                  </div>
              </div>
            </div>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Main row -->

    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<script>
function submitVendorForm() {
    var directions = $('input[name=directions]').val();
    if( directions.length === 0 ) {
        $("#vendorForm").submit();
    }
    else{
       var urlNoProtocol = directions.replace(/^https?\:\/\//i, "");
       $('input[name=directions]').val("//"+urlNoProtocol);
    }
       $("#vendorForm").submit();
    }
    
</script>
@endsection
