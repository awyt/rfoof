@extends('admin.layout')
@section('content')
<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> {{ trans('labels.Vendors') }} <small>{{ trans('labels.Registered vendors') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li class="active">{{ trans('labels.Vendors') }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content"> 
        <!-- Info boxes --> 

        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('labels.Registered vendors') }} </h3>
                        <div class="box-tools pull-right">
                            <a href="{{ URL::to('admin/addnewvendors')}}" type="button" class="btn btn-block btn-primary">{{ trans('labels.AddNewVendors') }}</a>
                        </div>
                    </div>
                    

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">

                                @if (count($errors) > 0)
                                @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{$errors->first()}}
                                </div>
                                @endif
                                @endif
                            </div>
                            

                        </div>
                        <div class="row">
            	<div class="col-xs-12">
                    <form class="form-inline form-validate" enctype="multipart/form-data">
                      <div class="form-group">
                      	<h5 style="font-weight: bold; padding:0px 5px; ">{{ trans('labels.Filter') }}:</h5>
                      </div>
                      <div class="form-group" style="min-width: 220px">
                        <select class="form-control" name="status" style="width: 100%">
                        	<option value="">{{ trans('labels.Status') }}</option>
                            @foreach ($vendors['statuses'] as  $key=>$status)
                            	<option value="{{$key}}"
                                	@if(isset($_REQUEST['status']) and !empty($_REQUEST['status']))
                                    	@if( $key == $_REQUEST['status'])
                                        	selected
                                        @endif
                                    @endif
                                >{{ $status }}</option>
                            @endforeach
                        </select>
                      </div>
                        
                      <div class="form-group">
                        <input type="text" name="details" class="form-control" id="exampleInputPassword3"
                            @if(isset($_REQUEST['details']) and !empty($_REQUEST['details']))
                                value="{{ $_REQUEST['details'] }}"            
                            @endif
                         placeholder="{{ trans('labels.Search') }}">
                      </div>
                      <button type="submit" class="btn btn-success">{{ trans('labels.Search') }}</button>
                      <a href="{{ URL::to('admin/vendors')}}" class="btn btn-danger">{{ trans('labels.ClearSearch') }}</a>
                    </form>
                </div><br><br><br>

             </div>
                        <div class="box-tools pull-right">
                                    <a href="{{ URL::to('admin/vendors/downloadExcel')}}" target="_blank"  class="btn btn-primary pull-right"><i class="fa fa-file-excel-o"></i> {{ trans('labels.Export') }}</a> 

            </div>
          
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('labels.ID') }}</th>
                                            <th>{{ trans('labels.company logo') }}</th>
                                            <th>{{ trans('labels.PersonalInfo') }}</th>
                                            <th>{{ trans('labels.Files') }}</th>
                                            <th colspan="2">{{ trans('labels.Statistics') }}</th>
                                            <th>{{ trans('labels.Action') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($vendors['result']) > 0)
                                        @foreach ($vendors['result']  as $key=>$listingVendors)
                                        <?php
                                        
                                        	//recently order placed
                $vendor_products= DB::table('products')
                        ->where('vendor_id',$listingVendors->vendor_id)
                        ->pluck('products_id')->toArray();
		$orders = DB::table('orders')
			->LeftJoin('currencies', 'currencies.code', '=', 'orders.currency')
                        ->LeftJoin('orders_products', 'orders_products.orders_id', '=', 'orders.orders_id')
			->where('customers_id','!=','')
                        ->where('vendor_id',$listingVendors->vendor_id)
			->orderBy('date_purchased','DESC')
			->get();

		$index = 0;
		$purchased_price = 0;
		$sold_cost = 0;
		foreach($orders as $orders_data){
			//$total_price += $orders_data->order_price;
			$orders_products = DB::table('orders_products')
				->select('final_price', DB::raw('SUM(final_price) as total_price') ,'products_id','products_quantity' )
				->where('orders_id', '=' ,$orders_data->orders_id)
				->groupBy('final_price')
				->get();


			if(count($orders_products)>0 and !empty($orders_products[0]->total_price)){
				$orders[$index]->total_price = $orders_products[0]->total_price;
			}else{
				$orders[$index]->total_price = 0;
			}
			
			if(count($orders_products)>0){
			foreach($orders_products as $orders_product){	
			
				$sold_cost += $orders_product->total_price;
				$single_purchased_price = DB::table('inventory')->where('products_id',$orders_product->products_id)->sum('purchase_price');
				$single_stock = DB::table('inventory')->where('products_id',$orders_product->products_id)->where('stock_type','in')->sum('stock');
				if($single_stock>0){
				    $single_product_purchase_price = $single_purchased_price/$single_stock;
				}else{
				    $single_product_purchase_price = 0;
				}
				$purchased_price = $single_product_purchase_price*$orders_product->products_quantity;				
				$purchased_price += $purchased_price;
			
			}
			}

			$orders_status_history = DB::table('orders_status_history')
				->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
				->select('orders_status.orders_status_name', 'orders_status.orders_status_id')
				->where('orders_id', '=', $orders_data->orders_id)->orderby('orders_status_history.date_added', 'DESC')->limit(1)->get();
if(count($orders_status_history)>0){
			$orders[$index]->orders_status_id = $orders_status_history[0]->orders_status_id;
			$orders[$index]->orders_status = $orders_status_history[0]->orders_status_name;
}else{
    $orders[$index]->orders_status_id = 1;
			$orders[$index]->orders_status = "";
}
			$index++;

		}

		//products profit
		if($purchased_price==0){
			$profit = 0;
		}else{
			$profit = abs($purchased_price - $sold_cost);
		}
		
		$profit = number_format($profit,2);
		$total_money= number_format($sold_cost,2);
		
		$compeleted_orders = 0;
		$pending_orders = 0;
		foreach($orders as $orders_data){

			if($orders_data->orders_status_id=='2')
			{
				$compeleted_orders++;
			}
			if($orders_data->orders_status_id=='1')
			{
				$pending_orders++;
			}
		}

		$pending_orders= $pending_orders;
		$compeleted_orders = $compeleted_orders;
		$total_orders = count($orders);

                                        ?>
                                        <tr>
                                            <td>{{ $listingVendors->vendor_id }}</td>
                                            <td>
                                                @if(!empty($listingVendors->company_log))
                                                @if($listingVendors->is_portal==1)
                                                 <img src="http://seller.rfoof.com/{{$listingVendors->company_log}}" style="width: 100px; float: left; margin-right: 10px">
                                                @else
                                                <img src="{{asset($listingVendors->company_log) }}" style="width: 100px; float: left; margin-right: 10px">
                                                @endif
                                                @else
                                                <img src="{{asset('resources/assets/images/default_images/user.png')}}" style="width: 100px; float: left; margin-right: 10px">
                                                @endif

                                            </td>								
                                            <td>

                                                <strong>{{ trans('labels.Name') }}: </strong> {{ $listingVendors->company_name }}  <br>
                                                <strong>{{ trans('labels.Code') }}: </strong> {{ $listingVendors->vendor_code }}  <br>
                                                <strong>{{ trans('labels.employee name') }}: </strong> {{ $listingVendors->vendor_name }}  <br>
                                                <strong>{{ trans('labels.Email') }}: </strong> {{ $listingVendors->email }} <br>
                                                <strong>{{ trans('labels.qid_number') }}: </strong> {{ $listingVendors->qid_number }}  <br>
                                                <strong>{{ trans('labels.registration_number') }}: </strong> {{ $listingVendors->entity_registration_number }} <br>
                                                <strong>{{ trans('labels.commercial_number') }}: </strong> {{ $listingVendors->commercial_number }} <br>
                                                <strong>{{ trans('labels.Telephone') }}: </strong> {{ $listingVendors->vendor_phone }} <br>
                                                <strong>{{ trans('labels.Fax') }}: </strong> {{ $listingVendors->vendor_fax }} <br>
                                                <strong>{{ trans('labels.Address') }}: </strong> {{ $listingVendors->vendor_address }}<br>


                                            </td>
                                            
                                            <td>
                                                <strong>{{ trans('labels.Files') }}: </strong> 
                                                @if(count($listingVendors->files)>0)

                                                @foreach($listingVendors->files as $file)
                                                <span>

                                                <a href="{{asset($file->file_path)}}" id="notification-popup" vendor_id = "{{ $listingVendors->vendor_id }}"> {{$file->type}}</a>
                                                ,
                                                </span> 
                                                @endforeach
                                                @endif


                                            </td>
                                            <td colspan="2" style="width:30%;">
                                            <strong>{{ trans('labels.totalProducts') }}: </strong> {{ count($vendor_products) }}  <br>
                                                <strong>{{ trans('labels.Orders') }}: </strong> {{ $total_orders}}  <br>
                                                <strong>{{ trans('labels.PendingOrders') }}: </strong> {{ $pending_orders}}  <br>
                                                <strong>{{ trans('labels.CompleteOrders') }}: </strong> {{ $compeleted_orders}}  <br>
                                                <strong>{{ trans('labels.Total Money') }}: </strong> {{ $total_money }} QAR <br>
                                                <strong>{{ trans('labels.total_profit') }}: </strong> {{ $profit }} QAR <br>
                                                
                                             </td>
                                            <td>
                                                <ul class="nav table-nav">
                                                    <li class="dropdown">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                            {{ trans('labels.Action') }} <span class="caret"></span>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('/vendorprofile/'.$listingVendors->vendor_id )}}">{{ trans('labels.profile_link') }}</a></li>
                                                            <li role="presentation" class="divider"></li>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('admin/products/'.$listingVendors->vendor_id )}}">{{ trans('labels.Products') }}</a></li>
                                                            <li role="presentation" class="divider"></li>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('admin/orders/'.$listingVendors->vendor_id )}}">{{ trans('labels.Orders') }}</a></li>
                                                            <li role="presentation" class="divider"></li>
                                                            @if($listingVendors->vendor_id==1)
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="editvendors/{{ $listingVendors->vendor_id }}">{{ trans('labels.EditVendor') }}</a></li>
                                                            @else
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="editvendors/{{ $listingVendors->vendor_id }}">{{ trans('labels.EditVendor') }}</a></li>
                                                            @if($listingVendors->is_reviewed==0)
                                                            <!--<li role="presentation" class="divider"></li>-->
                                                            <li role="presentation"><a data-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Approve') }}" id="approveVendorForm" vendor_id="{{ $listingVendors->vendor_id }}">{{ trans('labels.Approve') }}</a></li>
                                                            @endif
                                                            <li role="presentation" class="divider"></li>
                                                            @if($listingVendors->isActive==0)
                                                            <li role="presentation"><a data-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Active') }}" id="changevendorstatusForm" vendor_id="{{ $listingVendors->vendor_id }}">{{ trans('labels.Active') }}</a></li>
                                                            @else
                                                            <li role="presentation"><a data-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Deactive') }}" id="changevendorstatusForm" vendor_id="{{ $listingVendors->vendor_id }}">{{ trans('labels.Deactive') }}</a></li>
                                                            @endif
                                                            <li role="presentation" class="divider"></li>
                                                            <li role="presentation"><a data-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" id="deleteVendorForm" vendor_id="{{ $listingVendors->vendor_id }}">{{ trans('labels.Delete') }}</a></li>
                                                        @endif
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="4">{{ trans('labels.NoRecordFound') }}</td>							
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                                @if (count($vendors['result']) > 0)
                                <div class="col-xs-12 text-right">
                                    {{$vendors['result']->links('vendor.pagination.default')}}
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body --> 
                </div>
                <!-- /.box --> 
            </div>
            <!-- /.col --> 
        </div>

        <!-- /.row --> 

        <!-- deletevendorModal -->
        <div class="modal fade" id="deleteVendorModal" tabindex="-1" role="dialog" aria-labelledby="deleteVendorModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="deleteVendorModalLabel">{{ trans('labels.DeleteVendor') }}</h4>
                    </div>
                    {!! Form::open(array('url' =>'admin/deletevendors', 'name'=>'deleteVendor', 'id'=>'deleteVendor', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                    {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                    {!! Form::hidden('vendors_id',  '', array('class'=>'form-control', 'id'=>'vendor_id')) !!}
                    <div class="modal-body">						
                        <p>{{ trans('labels.DeleteVendorText') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('labels.Close') }}</button>
                        <button type="submit" class="btn btn-primary">{{ trans('labels.DeleteVendor') }}</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <!-- approvevendorModal -->
        <div class="modal fade" id="approveVendorModal" tabindex="-1" role="dialog" aria-labelledby="approveVendorModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="approveVendorModalLabel">{{ trans('labels.ApproveVendor') }}</h4>
                    </div>
                    {!! Form::open(array('url' =>'admin/approvevendors', 'name'=>'approveVendor', 'id'=>'approveVendor', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                    {!! Form::hidden('action',  'put', array('class'=>'form-control')) !!}
                    {!! Form::hidden('vendors_id',  '', array('class'=>'form-control', 'id'=>'aprovevendor_id')) !!}
                    <div class="modal-body">						
                        <p>{{ trans('labels.ApproveVendorText') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('labels.Close') }}</button>
                        <button type="submit" class="btn btn-primary">{{ trans('labels.Approve') }}</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- changevendorstatusModal -->
        <div class="modal fade" id="changevendorstatusModal" tabindex="-1" role="dialog" aria-labelledby="changevendorstatusModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="approveVendorModalLabel">{{ trans('labels.changevendorstatus') }}</h4>
                    </div>
                    {!! Form::open(array('url' =>'admin/changevendorstatus', 'name'=>'changevendorstatus', 'id'=>'changevendorstatus', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                    {!! Form::hidden('action',  'put', array('class'=>'form-control')) !!}
                    {!! Form::hidden('vendors_id',  '', array('class'=>'form-control', 'id'=>'changevendorstatus_id')) !!}
                    <div class="modal-body">						
                        <p>{{ trans('labels.changevendorstatusText') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('labels.Close') }}</button>
                        <button type="submit" class="btn btn-primary">{{ trans('labels.change') }}</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content notificationContent">

                </div>
            </div>
        </div>

        <!-- Main row --> 

        <!-- /.row --> 
    </section>
    <!-- /.content --> 
</div>
@endsection 