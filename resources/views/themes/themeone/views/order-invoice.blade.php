<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta charset="utf-8">
<style>
@import url('https://fonts.googleapis.com/css?family=Tajawal&display=swap');
</style>
<style type="text/css">
   body{
       font-family: 'DejaVuSans-Bold', sans-serif; 
   }
</style>
    <body style="border: thin;
    border-style: solid;">
    <div class="body-wrapper">
       <table class="header">
            <tr>
                <td>@if(!empty($orders->vendor_image))
                <img height="100px"; src="{{asset($orders->vendor_image)}}">
                @else
                <img src="{{asset('resources/assets/images/default_images/user.png')}}" >
                @endif
                </td>
            </tr>

            <tr>
                <td>
                    <h4>
                    Important note : Kindly show up the created pdf file to the seller to take the advantages of our offers.
                    </h4>
                </td>
            </tr>

        </table>



        <table class="seller-info-contact-table" border="1" style="width: 100%;border: 1px #000 solid;">
            <thead style="background-color:#e6eaf0;">
            <tr>
                <th>
                    Supplier: {{$orders->vendor_name or ''}}
                </th>
                <th>
                    CLIENT: {{$orders->customers_firstname or ''}} {{$orders->customers_lastname or ''}}
                </th>

            </tr>
            </thead>

            <tbody>
            <tr>
                <td>Rep.: {{$orders->employee or ''}} </td>
                <td>Order ID: {{$orders->orders_id or ''}}</td>
            </tr>
            <tr>
                <td>Mob: {{$orders->vendor_phone or ''}}</td>
                <td>Mob: {{$orders->customers_telephone or ''}}</td>
            </tr>
            <tr>
                <td>Address: {{$orders->vendor_address or ''}}</td>
                <td>Date : {{$orders->date_purchased or ''}}</td>
            </tr>
            <tr>
                <td>Email : {{$orders->vendor_email or ''}}</td>
                <td>Email : {{$orders->email or ''}}</td>
            </tr>
            </tbody>
        </table>

        <!-- end seller info-->

        <!-- start quotation-section -->

        <br><br/>
            <!--seller-msg-->
            <table class="seller-msg">
                <thead>
                    <tr>
                        <th style="font-size:large;margin-left: 50%">Quotation</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><p>Dear, Sir</p>
                            Thank you for your inquiry and choosing {{$orders->vendor_name or ''}} . Kindly find below the
                            requested quotation for Suppling building materials :</td>
                    </tr>
                </tbody>
            </table>
            <!--end seller-msg-->
<br><br/>
            <table class="quotation-details" border="1" style="width: 100%;border: 2px #000 solid;" cellpadding="10" cellspacing="5">
                <thead style="background-color:#e6eaf0;">
                    <tr>
                        
                        <th>Code</th>
                        <th>Item</th>
                        <th>QTY</th>
                        <th>PRICE/UNIT</th>
                        <th>TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $price=0; ?>
                    @if(!empty($orders->products))
                    @foreach($orders->products as $key=>$product)
                    <?php 
                                                    $price+= $product->final_price;					
                                                ?>
                    <tr>
                        
                        <td>{{$product->product_code}}</td>
                        <td style="width:50%">{{$product->products_name}}</td>
                       
                        <td>{{$product->products_quantity}}</td>
                        <td>{{number_format($product->products_price)}} QAR</td>
                        <td>{{number_format($product->products_price * $product->products_quantity)}} QAR</td>
                    </tr>
                    @endforeach
                    @endif
                    <tr>
                        <td colspan="4"> Grand Total</td>
                        <td>{{number_format($price)}} QAR</td>
                    </tr>

                </tbody>
            </table>

        <!-- end quotation-section -->


<br><br/>
        <!--start terms-conditions-->
        <table class="terms-conditions">
            <tbody>

                <tr>
                    <td>
                        <h4 style="font-size:large;text-decoration: underline;">TERMS AND CONDITIONS :-</h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ol>
                            <li>
                                
                                    Kindly show up the created pdf file to the seller to take the advantages of our offers.
                                
                            </li>

                            <li>
                               
                                    Please inform us if there is any changes for the prices which we published on our platform.
                                
                            </li>
                            <li>
                                
                                    Please make sure that the seller is providing for you the same quilty as per the specs file on our platforms.
                                
                            </li>
                        </ol>

                    </td>
                </tr>

                <tr>
                    <td>
                        <p>
                            If there is any question, please do not hesitate to contact us. Note that the above quotation is valid for a period of 3
                            days
                        </p>
                        <p><b>This quotation provided by <a href="http://rfoof.com">Rfoof.com</a>, The first construction marketplace in world.</b></p>

                    </td>
                </tr>
            </tbody>

        </table>
        <!--end terms-conditions-->
        <br><br>
        <table class="copy-rights">

            <tbody>
                <tr>
                    <td><img width="200px;" src="{{asset($orders->rfoof_logo) }}"></td>
                    <td><table class="copy-rights">

            <tbody>
         
                <tr>
                    <td>

                        <p>
                            Mobile: <a href="tel:0097450799030">0097450799030</a> <br>
                            Land Line: <a href="tel:0097444769914">0097444769914</a> <br>
                            Fax : 0097444769900 <br>
                            Email : <a href="mailto:info@rfoof.com">info@rfoof.com</a> <br>
                            Address : <br>
                            2ND FLOOR,BLOCK 4&6 ARKAN BUILDING,<br> 
                            BARWA COMMERCIAL AVENUE, ABU HAMOUR,<br> 
                            DOHA, QATAR.
                        </p>
                
                    </td>
                </tr>
            </tbody>



        </table>
 </td>
                </tr>
            </tbody>



        </table>
 



    </div>
</body>
</html>