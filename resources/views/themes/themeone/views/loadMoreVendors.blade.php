<!--vendors-list-->

@if($result['vendors']['success']==1)

        
@foreach($result['vendors']['vendors'] as $vendor)

    <div class="vendor">
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-2 ">
                                <div class="vendor-logo">
                                    <!-- default image -->
                                    <div class="">
                                        @if($vendor->is_portal==1)
                                        <img class="img-fluid" src="http://seller.rfoof.com/resources/assets/images/vendors/{{$vendor->company_log}}" >
                                        @else
                                        <img class="img-fluid" src="{{asset($vendor->company_log) }}" alt="vendor-logo">
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-12 col-lg-10">


                                <div class="vendor-data">

                                    <h3 class="vendor-data-title">{{$vendor->company_name}}</h3>
                                    <hr>
                                    <div class="vendor-info">
                                        <div class="vendor-info-statistic">
                                            <?php
                                            $sales = DB::table('orders')->where('vendor_id', $vendor->vendor_id)
                                                            ->LeftJoin('orders_status_history', 'orders_status_history.orders_id', '=', 'orders.orders_id')
                                                            ->where('orders_status_history.orders_status_id', 2)->get();
                                            $products = DB::table('products')->where('vendor_id', $vendor->vendor_id)->get();
                                            ?>
                                            <span href="#" >@lang('website.nosales') <span class="vendor-info-statistic-num">({{count($sales)}})</span></span>
                                            <!--<span href="#">seller rate<span class="vendor-info-statistic-num">(2)</span></span>-->
                                            <span href="#">@lang('website.noproducts') <span class="vendor-info-statistic-num">({{count($products)}})</span></span>

                                        </div>
                                        <ul class="vendor-data-contact-list">
                                            <li>
                                                <span> <i class="fa fa-map-marker" aria-hidden="true"></i>  {{$vendor->vendor_address}}</span>
                                                @if(!empty($vendor->directions))
                                                <a href="{{$vendor->directions}}"<span>get direction <i class="fa fa-map-signs" aria-hidden="true"></i></span>
                                                    @endif
                                            </li>

                                            <li>
                                                <span> <i class="fa fa-phone" aria-hidden="true"></i> @lang('website.company phone') : {{$vendor->vendor_phone}}</span>
                                                <span><i class="fa fa-fax" aria-hidden="true"></i> @lang('website.company fax') : {{$vendor->vendor_fax}}</span>
                                                <a href="{{url('/vendorprofile/'.$vendor->vendor_id)}}"> <button class="vendor-details-btn btn float-right">details</button></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

@endforeach

@elseif(count($result['vendors']['vendors'])== 0 or $result['vendors']['success']==0 or count($result['vendors']['vendors']) < $result['limit'])
    <style>
        #load_vendors{
            display: none;
        }
        #loaded_content{
            display: none !important;
        }
        #loaded_content_empty{
            display: block !important;
        }
    </style>
@endif
