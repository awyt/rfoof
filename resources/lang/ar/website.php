<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
		
	'bannerLabel1'=>'الشحن مجانا',
		
	'bannerLabel1Text'=>'على الطلبات أكثر من 1000 درهم ',
		
	'bannerLabel2'=>'الاسترداد النقدي',
		
	'bannerLabel2Text'=>'خلال 30 يوماً من تاريخ الطلب',
		
	'bannerLabel3'=>'دعم 24/7',
		
	'hotline'=>'الخط الساخن',
	
	'bannerLabel4'=>'الدفع الآمن',
		
	'bannerLabel4Text'=>'حماية الدفع عبر الإنترنت',
		
	'TopSales'=>'الأكثر طلباً',
		
	'Special'=>'مميز',
		
	'MostLiked'=>'الأكثر تفضيلاً',
		
	'New'=>'جديد',
		
	'Likes'=>'التفضيلات',
		
	'Welcome to our Store'=>'أهلأ بكم في متجر رفوف',
		
	'Orders'=>'الطلبات',
		
	'All Categories'=>'جميع التصنيفات',
		
	'Search entire store here'=>'ما الذى تبحث عنه؟',
	'Search entire vendors here'=>'ما الذى تبحث عنه؟',
	'Search'=>'ابحث',
	'Vendors'=>'البائعين',
	'Store'=>'المتجر',
    	
	'Profile'=>'الملف الشخصي',
		
	'Logout'=>'الخروج',
		
	'Login/Register'=>'دخول / تسجيل',
		
	'item(s)'=>'الأصناف',
		
	'Quantity'=>'الكمية',
		
	'Total Items'=>'مجموع الأصناف',
		
	'Total Price'=>'السعر الإجمالي',
		
	'View Cart'=>'عرض العربة',
		
	'Checkout'=>'الدفع',
		
	'Menu'=>'القائمة',	
		
	'My Cart'=>'عربة التسوق',
		
	'footer text'=>'خدمة الجزر أبجد هوز. ومع ذلك، ليس هناك ما كان عليه سابقا. ومع ذلك، فإن وثيقة الهوية الوحيدة كبير والحنجرة وحتى المؤلف من البروتين. في أي كرة السلة سلطة يصبح قوس واحدة، والتخمير العزيز.',
		
	'Helpful Links'=>'روابط مفيدة',
		
	'Terms'=>'الشروط والأحكام',
		
	'Privacy'=>'الخصوصية',
		
	'Discount'=>'خصم',
		
	'About Us'=>'عن رفوف',
		
	'Contact Us'=>'اتصل بنا',
		
	'Terms & Condtions'=>'الشروط والأحكام',
		
	'Wishlist'=>'قائمة الرغبات',
		
	'Shopping Cart'=>'عربة التسوق',
		
	'Privacy Policy'=>'سياسة الخصوصية',
		
	'Phone'=>'الهاتف',
		
	'Email'=>'البريد الإلكتروني',
		
	'Copy Rights'=>'جميع الحقوق محفوظة © رفوف للحلول المتقدمة',
		
	'From our News'=>'موضوعات مختارة من مدونة رفوف',
		
	'View All News'=>'عرض جميع الموضوعات',
		
	'Readmore'=>'اقرأ المزيد',
		
	'Weekly'=>'أسبوعي',
		
	'banner saving text'=>'خصومات تصل إلى 20% لنهاية الاسبوع',
		
	'Shop now'=>'تسوق الآن',
		
	'Sale'=>'تخفيضات',
		
	'Top Selling of the Week'=>'الأكثر طلباً لهذا الأسبوع',
		
	'View Detail'=>'عرض التفاصيل',
		
	'Newest Products'=>'أحدث المنتجات',
		
	'View All'=>'عرض االجميع',
	
	'Special'=>'مميز',
		
	'items'=>'منتجات',
	'item'=>'منتج',	
		
	'SubTotal'=>'المجموع',		
		
	'Couponisappliedsuccessfully'=>'تم قبول قسيمة الشراء وتطبيق الخصم!',
		
	'You are not allowed to use this coupon'=>'القسيمة مرفوضة',
		
	'This coupon has been reached to its maximum usage limit'=>'تم الوصول إلى الحد الأقصى لاستخدام هذه القسيمة',
		
	'coupon is used limit'=>'تم الوصول إلى حد الاستخدام للكوبون',
		
	'Coupon amount limit is low than minimum price'=>'حد مبلغ الكوبون منخفض عن السعر الأدنى.',
		
	'Coupon amount limit is exceeded than maximum price'=>'يتم تجاوز حد مبلغ الكوبون عن الحد الأقصى للسعر.',
		
	'Coupon cannot be applied this product is in sale'=>'لا يمكنك استخدام كوبون الخصم مع منتجات مخفضة السعر بالفعل',
		
	'Coupon amount is greater than total price'=>'قيمة الكوبون أكبر من السعر الإجمالي.',
		
	'Coupon amount is greater than product price'=>'قيمة الكوبون أكبر من سعر المنتج.',
		
	'Coupon does not exist'=>'الكوبون غير موجود أو رمز قسيمة غير صالح.',
		
	'Please enter a valid coupon code'=>'يرجى إدخال كود كوبون صالح',
		
	'Coupon is already applied'=>'الكوبون مطبق بالفعل',
		
	'Cart item has been deleted successfully'=>'تم حذف المنتج من عربة التسوق  بنجاح!',
		
	'Coupon has been removed successfully'=>'تمت إزالة الكوبون بنجاح!',
		
	'Cart has been updated successfully'=>'تم تحديث عربة التسوق بنجاح!',
		
	'Coupon can not be apllied to empty cart'=>'لا يمكن تطبيق القسيمة على عربة فارغة.',
		
	'cartEmptyText'=>'لا يوجد لديك منتجات في عربة التسوق الخاصة بك.<br>انقر <a href="shop">هنا</a> لمواصلة التسوق.',
		
	'Home'=>'الصفحة الرئيسية',
		
	'Shopping cart'=>'عربة التسوق',
		
	'Poducts'=>'المنتجات',
		
	'Detail'=>'التفاصيل',
		
	'Quantity'=>'الكمية',
		
	'Unit Price'=>'سعر الوحدة',
		
	'Item Total'=>'إجمالي الصنف',
		
	'Special'=>'مميز',
		
	'Coupon Code'=>'رمز القسيمة',
		
	'Checkout'=>'تأكيد الطلب',
		
	'Update Cart'=>'تحديث العربة',
		
	'Back To Shopping'=>'العودة إلى التسوق',
		
	'Total'=>'الإجمالي',
		
	'Discount(Coupon)'=>'خصم (قسيمة)',
		
	'Order Summary'=>'ملخص الطلب',
		
	'Subtotal'=>'المجموع',
		
	'Apply'=>'تطبيق',
		
	'Please enter your first name'=>'الرجاء إدخال اسمك الأول.',
		
	'Please enter your last name'=>'الرجاء إدخال اسمك الأخير.',
		
	'Login'=>'تسجيل الدخول',
		
	'Create An Account'=>'إنشاء حساب',
		
	'Personal Information'=>'المعلومات الشخصية',
		
	'Error'=>'خطأ',
		
	'Success'=>'تم',
		
	'First Name'=>'الاسم الأول',
		
	'Last Name'=>'اسم العائلة',
		
	'Email Adrress'=>'البريد الإلكتروني ',
		
	'Please enter your valid email address'=>' الرجاء إدخال عنوان بريدك الإلكتروني الصحيح.',
		
	'Gender'=>'الجنس',
		
	'Please select your gender'=>'رجاء اختيار الجنس.',
		
	'Picture'=>'صورة',
		
	'Password'=>'كلمة المرور',
		
	'Please enter your password'=>'الرجاء إدخال كلمة المرور الخاصة بك.',
		
	'Confirm Password'=>'تأكيد كلمة المرور',
		
	'Please re-enter your password'=>' يرجى إعادة إدخال كلمة المرور الخاصة بك. ',
		
	'Password does not match the confirm password'=>'كلمتي المرور غير متطابقتان',
		
	'Creating an account means you are okay with our'=>'بإنشاء حسابك على متجر رفوف فإنك توافق على ',
	
	'Terms and Services'=>'الشروط والخدمات',
		
	'Privacy Policy'=>'سياسة الخصوصية',
		
	'Refund Policy'=>'سياسة الاسترداد النقدي',
		
	'Please accept our terms and conditions'=>'"يرجى قبول الشروط والأحكام',
		
	'Sign Up'=>'التسجيل',
		
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit'=>'أبجد هوز دولور الجلوس امات، إيليت.',
		
	'Duis at nisl luctus, malesuada diam non, mattis odio'=>'لاعبي التزلج على الجليد البلوزات،  بقطر ليس هناك الكثير من الأجهزة.',
		
	'Fusce porta neque at enim consequat, in vulputate tellus faucibus'=>'ولكن، أقول لكم، ولا Fusce بورتا الجلوس امات، hendrerit في vulputate إيليت faucibus.',
		
	'Pellentesque suscipit tortor id dui accumsan varius'=>'أن درجة الحرارة الترويجي طبقة دوى الكازينو.',
		
	'Sed interdum purus imperdiet tortor imperdiet, et ultricies leo gravida'=>'لكن في بعض الأحيان الفلفل الحار التمويل التمويل الكلي وultricies الأسد حاملا.',
		
	'Aliquam pharetra urna vel nulla egestas, non laoreet mauris mollis'=>'Aliquam URNA pharetra، أو حيث لا إنفاذ القانون، غير laoreet mauris موليس.',
		
	'Integer sed velit sit amet quam pharetra ullamcorper'=>'صحيح velit سد الجلوس quam امات ullamcorper pharetra.',
		
	'Proin eget nulla accumsan, finibus lacus aliquam, tincidunt turpis'=>'tincidunt Phasellus، UT accumsan augue، نهايات aliquam lacus، أبجد ميلان ليو.',
		
	'Nam at orci tempor, mollis mi ornare, accumsan risus'=>'لكنه السريرية طويلة، لينة، يا طبقة اعتصام الضحك.',
		
	'Cras vel ante vel augue convallis posuere'=>'غدا قبل أو مجموعة بورتا.',
		
	'Ut quis dolor accumsan, viverra neque nec, blandit leo'=>'بالنسبة لشخص ل، viverra neque غير المصنفة في موضع الألم المنحى، blandit تيمبوس ليو.',
		
	'Checkout'=>'تأكيد الشراء',
		
	'Shopping cart'=>'عربة التسوق',
		
	'Billing Address'=>'عنوان الدفع',
		
	
	'Payment Methods'=>'طرق الدفع',
		
	'Order Detail'=>'تفاصيل الطلب',
		
	'Company'=>'المؤسسة',
		
	'Address'=>'العنوان',
		
	'Country'=>'الدولة',
		
	'State'=>'الولاية أو المحافظة',
		
	'City'=>'المدينة',
		
	'Zip/Postal Code'=>'الرمز البريدي',
		
	'Continue'=>'استكمال',
		
	'Please enter your company name'=>'الرجاء إدخال اسم شركتك.',
		
	'Please enter your address'=>'الرجاء إدخال عنوانك.',
		
	'Please select your country'=>' يرجى تحديد الدولة.',
		
	'Please select your state'=>'يرجى تحديد الولاية  أو المحافظة الخاصة بك.',
		
	'Please enter your city'=>'الرجاء إدخال مدينتك.',
		
	'Please enter your Zip/Postal Code'=>'الرجاء إدخال الرمز البريدي',
		
	'Update'=>'تحديث',
		
	'Add Address'=>'إضافة عنوان',
		
	'Your address has been deteled successfully'=>'تم حذف العنوان بنجاح!',
		
	'Your address has been chnaged successfully'=>'لقد تم تغيير عنوانك بنجاح!',
		
	'Default'=>'افتراضي',
		
	'Address Info'=>'معلومات العنوان',
		
	'Action'=>'العمل',
		
	'Your address has been updated successfully'=>' تم تحديث عنوانك بنجاح!',
		
	'Same shipping and billing address'=>'عنوان الشحن والدفع نفسه.',
		
	'Select Country'=>'اختر الدولة',
		
	'Select State'=>'اختر ولايه',
		
	'Other'=>'أخرى',
		
	'Shipping Methods'=>'طرق الشحن',
		
	'Confirm Order'=>'تأكيد الطلب',
		
	'Order Now'=>'اطلب الآن',
		
	'Please select a prefered shipping method to use on this order'=>'يُرجى تحديد طريقة شحن مفضلة لاستخدامها في هذا الطلب.',
		
	'My Orders'=>'طلباتك',
		
	'No order is placed yet'=>'لم يتم تقديم أي طلب بعد.',
		
	'Order ID'=>'معرف الطلب',
		
	'Quantity'=>'الكمية',
		
	'Order Date'=>'تاريخ الطلب',
		
	'Price'=>' الأسعار',
		
	'View Order'=>'التفاصيل',
		
	'Status'=>'الحالة',
		
	'Order information'=>'معلومات الطلب',
		
	'Please select a prefered shipping method to use on this order'=>'يُرجى تحديد طريقة شحن مفضلة لاستخدامها في هذا الطلب.',
		
	'Customer Detail'=>'تفاصيل العميل',
		
	'Admin Comments'=>'تعليقات المشرف',
		
	'Order Comments'=>'تعليقات الطلبات',
		
	'Comments'=>'تعليقات',
		
	'Shipping Cost'=>'تكلفة الشحن',
		
	'Tax'=>'الضرائب',
		
	'Payment Method'=>'طريقة الدفع',
		
	'Shipping Method'=>'طريقة الشحن',
		
	'Payment/Shipping Method'=>'وسيلة الدفع / الشحن',
		
	'Billing Detail'=>'تفاصيل الدفع',
		
	'Shipping Detail'=>'تفاصيل الشحن',
		
	'Pay'=>'الدفع',
		
	'Your location does not support this'=>' موقعك لا يدعم هذا',
		
	'Please select your shipping method'=>'الرجاء تحديد طريقة الشحن الخاصة بك.',
		
	'Please select a prefered payment method to use on this order'=>'يرجى تحديد طريقة دفع مفضلة لاستخدامها في هذا الطلب',
		
	'Please select your payment method'=>' يرجى تحديد طريقة الدفع الخاصة بك.',
		
	'BrainTree Payment'=>'BrainTree الدفع',
		
	'Card number'=>'رقم البطاقة',
		
	'Expiration'=>'تاريخ الانتهاء',
		
	'CVC'=>'CVC',
		
	'Payment successful'=>'تم الدفع بنجاح',
		
	'Thanks You Your payment has been processed successfully'=>'شكرًا لك! تمت معالجة دفعتك بنجاح.',
		
	'Edit Cart'=>'تحرير سلة التسوق',
		
	'Full Name'=>'الاسم الكامل',
		
	'Email'=>'البريد الإلكتروني',
		
	'Message'=>'رسالة',
		
	'Our Address'=>'عنواننا',
		
	'contact us message'=>'شكرا على اتصالك بنا. سنتصل بك في غضون ثلاثة أيام عمل. ',
		
	'Subject'=>'الموضوع',
		
	'Send'=>'إرسال',
	
	'News Detail'=>'تفاصيل الأخبار',
	'Date'=>'التسجيل',
		
	'News'=>'الأخبار',
		
	'All News'=>'كل الأخبار',
		
	'Sort'=>'ترتيب',
		
	'Shop'=>'تسوق',
		
	'Newest'=>'الأحدث',
		
	'Price: High To Low'=>'الأعلى سعراً',
		
	'Price: Low To High'=>'الأقل سعراً',
		
	'Top Seller'=>'الأكثر طلباً',
		
	'Special Products'=>'منتجات مميزة',
		
	'Most Liked'=>'الأكثر تفضيلاً',
		
	'Display'=>'عرض',
		
	'Limit'=>'الحد',
		
	'No record found'=>'لم يتم العثور على أي عناصر',
		
	'All products are loaded.'=>'يتم عرض جميع المنتجات.',
		
	'All news are loaded'=>'تم تحميل جميع التدوينات.',
		
	'Load More'=>'عرض المزيد',
		
	'Oldest'=>'الأقدم',
		
	'News Categories'=>'فئات الأخبار',
		
	'A - Z'=>'أ - ي',
		
	'Z - A'=>'ي - أ',
		
	'Edit Address'=>'تعديل العنوان',
		
	'Add Address'=>'إضافة عنوان',
		
	'All products are loaded'=>' يتم عرض جميع المنتجات.',
		
	'Related Products'=>'منتجات ذات صلة ',
		
	'Products Description'=>'وصف المنتجات',
		
	'Order(s)'=>'طلبات الشراء',
		
	'Login Or Create An Account'=>'تسجيل الدخول أو إنشاء حساب',
		
	'New Customers'=>'عملاء جدد',
		
	'login page text for customer'=>'من خلال إنشاء حساب في متجرنا ، ستتمكن من التنقل عبر عملية الدفع بشكل أسرع ، وتخزين عناوين شحن متعددة ، وعرض وتتبع طلباتك في حسابك وأكثر.',
		
	'Dont have account'=>'لا تملك حساب؟',
		
	'or Sign in with'=>'أو تسجيل الدخول باستخدام',
		

	'Login with Facebook'=>'تسجيل الدخول باستخدام Facebook',
		
	'Login with Google'=>'تسجيل الدخول باستخدام Google',
		
	'Registered Customers'=>'العملاء المسجلين',
		
	'If you have an account with us, please log in'=>'إذا كان لديك حساب معنا ، يرجى تسجيل الدخول',
		
	'This field is required'=>'هذا الحقل مطلوب.',
		
	'Forgot Password'=>'هل نسيت كلمة المرور؟',
		
	'Please enter your password and should be at least 6 characters long'=>'الرجاء إدخال كلمة المرور الخاصة بك ويجب أن يكون طولها 6 أحرف على الأقل',
		
	'New Password'=>'كلمة المرور الجديدة',
		
	'Current Password'=>'كلمة المرور الحالية',
		
	'Please enter current password'=>' الرجاء إدخال كلمة المرور الحالية. ',
		
	'Please enter your valid phone number'=>'الرجاء إدخال رقم هاتف الصحيح. ',
		
	'Phone Number'=>'رقم الهاتف',
		
	'Please enter your date of birth'=>'الرجاء إدخال تاريخ ميلادك.',
		
	'Date of Birth'=>'تاريخ الميلاد',
		
	'Female'=>'أنثى',
		
	'Male'=>'ذكر',
		
	'Gender'=>'الجنس',
		
	'Ecommerce'=>'التجارة الإلكترونية',
		
	'Featured'=>'منتجات مميزة',
		
	'Filters'=>'خيارات البحث',
		
	'Apply'=>'تطبيق',
		
	'Reset'=>'إعادة ضبط',
		
	'Subscribe'=>'الاشتراك!',
		
	'Shipping Address'=>'عنوان الشحن',
		
	'Returned all products'=>'تمت إعادة جميع المنتجات.',
		
	'Search results empty'=>'نتائج البحث فارغة.',
		
	'Categories'=>'التصنيفات',
		
	'Product Detail'=>'تفاصيل المنتج',
		
	'Returned all filters successfully'=>'تمت إعادة خيارات البحث بنجاح.',
		
	'Filter is empty for this category'=>'لا تنطبق خيارات بحثك على أي عناصر في هذا التصنيف، جرب طريقة بحث أخرى',
		
	'Select Zone'=>'إختر المنطقة',
		
	'select Country'=>'اختر الدولة',
		
	'All Products'=>'جميع المنتجات',
		
	'Product is added'=>'تمت إضافة المنتج!',
		
	'Special products of the Week'=>'منتجات مميزة لهذا الأسبوع',
		
	'Forgot Password'=>'نسيت كلمة المرور',
		
	'Please Enter your email to recover your password'=>'الرجاء إدخال بريدك الإلكتروني لاسترداد كلمة المرور الخاصة بك.',
		
	'Qty'=>'الكمية',
		
	'You have no items in your shopping cart'=>' ليس لديك أي عناصر في سلة التسوق الخاصة بك.',
		
	'Password has been sent to your email address'=>' تم إرسال كلمة المرور إلى عنوان بريدك الإلكتروني.',
		
	'Email address does not exist'=>' عنوان البريد الإلكتروني غير موجود.',
		
	'Add to Cart'=>'إضافة إلى السلة',
		
	'Added'=>'تمت الإضافة!',
		
	'Subscribe for Newsletter'=>'اشترك في النشرة الإخبارية',
		
	'Your email address here'=>'عنوان بريدك الإلكتروني هنا ...',
		
	'Subscribe'=>'الاشتراك!',
		
	'About Store'=>'تواصل معنا',
		
	'Our Services'=>'خدماتنا',
		
	'Information'=>'المعلومات',
		
	'Popular Categories'=>'التصنيفات المشهورة',
		
	'Error while placing order'=>'خطأ أثناء تقديم الطلب. ',
		
	'Payment has been processed successfully'=>'تمت معالجة الدفعة بنجاح!',
		
	'Your order has been placed'=>'تم إرسال طلبك',
		
	'Email or password is incorrect'=>'البريد الإلكتروني أو كلمة المرور غير صحيحة!',
		
	'something is wrong'=>'هناك خطأ ما!',
		
	'Email already exist'=>'البريد الإلكتروني موجود بالفعل!',
		
		
	'ApplyCoupon'=>'تطبيق القسيمة',
		
	'proceedToCheckout'=>'تأكيد الطلب',
		
	'orderNotes'=>'ملاحظات الطلب',
	
	'Previous'=>'السابق',
	'Next'=>'التالي',
		
	'Out of Stock'=>'غير متوفر',
		
	'In stock'=>'المنتج متوفر',
		
	'Low in Stock'=>'على وشك النفاذ',
		
	'Edit'=>'تعديل',
		
	'Remove Item'=>'إزالة العنصر',
		
	'per page'=>'لكل صفحة ',
		
	'orderNotesandSummary'=>'ملاحظات الطلب والملخص',
		
	'Signup'=>'الاشتراك',
		
	'myProfile'=>'ملفي الشخصي',
		
	'orderID'=>'رقم تعريف الطلب',
		
	'orderStatus'=>'حالة الطلب',
		
	'Please enter your name'=>'الرجاء إدخال اسمك.',
		
	'Please enter your message'=>'الرجاء إدخال رسالتك.',
		
	'Prfile has been updated successfully'=>'تم تحديث الملف الشخصي بنجاح!',
		
	'Change Password'=>'تغيير كلمة المرور',
		
	'infoPages'=>'صفحات المعلومات',
		
	'homePages'=>'الصفحات الرئيسية',
		
	'homePage1'=>'الصفحات الرئيسية 1',
	'homePage2'=>'الصفحات الرئيسية 2',
	'homePage3'=>'الصفحات الرئيسية 3',
	'homePage4'=>'الصفحات الرئيسية 4',
	'homePage5'=>'الصفحات الرئيسية 5',
		
	'collection'=>'مجموعة',
		
	'hot'=>'رائج',
		
	'new in Stores'=>'المعروض حديثاً',
		
	'categroies'=>'الاقسام',
		
	'Call Us Free'=>'اتصل بنا',
		
	'Featured News'=>'التدوينات المميزة',
		
	'Upload Profile Photo'=>'تحميل صورة الملف الشخصي',
		
	'Choose...'=>' اختر ...',
		
	'Showing'=>'عرض',
		
	'of'=>'من',
		
	'results'=>'نتيجة.',
		
	'Password has been updated successfully'=>' تم تحديث كلمة المرور بنجاح! ',
		
	'Record not found'=>'لم يتم العثور على العنصر!',
	'contact us title'=>'تواصل معنا',
	'contact us description'=>'لا تتردد في التواصل معنا للإجابة على جميع أسئلتك واستفساراتك' ,
	'Follow Us'=>'تابعنا',
		
	'Shipping addresses are not added yet'=>'لم يتم إضافة عنوان الشحن.',
		
	'product is not added to your wish list'=>'لم يتم إضافة المنتج إلى قائمة المفضلة ',
		
	'Search result for'=>'نتيجة البحث عن',
		
	'item found'=>'تم العثور على عنصر',
	'cancel'=>'إلغاء',
	'Please write notes of your order'=>'يرجى كتابة ملاحظات عن الطلب',
	'Coupon Applied'=>'تم تطبيق الخصم',
	
	//new
	'Welcome'=>'أهلا بك',
	'Welcome Guest'=>'مرحبا !',	
	'Worldwide Express Plus'=>'إكسبرس بلاس',
	'Standard'=>'عادي',
	'Worldwide Expedited'=>'شحن مستعجل (أولوية)',
	'Worldwide Express'=>'شحن سريع',
	'2nd Day Air A.M.'=>'شحن جوي بعد يومين',
	'Next Day Air Early A.M.'=>'الشحن جوي اليوم التالي في وقت مبكر.',
	'Next Day Air Saver'=>'اليوم التالي اقتصادي',
	'3 Day Select'=>'اختيار 3 أيام',
	'Ground'=>'بري',
	'2nd Day Air'=>'طيران بعد يومين',
	'Next Day Air'=>'طيران اليوم التالي',
	'My Account'=>'حسابي',
	'Info Pages'=>'صفحات المعلومات',
	'Dummy Text'=>'لها أن جنوب أجزاء. ثم وفي اعتداء الجنوبي. تم الشرقي بمعارضة عدم, فقد أن حاول الآلاف إستعمل. مكن لعدم فهرست الخاطفة و, من تعديل لفرنسا الشهير عرض, أم مدينة السيطرة وصل. كان وقبل لعدم لم, جعل معزّزة والمانيا استطاعوا هو, مع بعض ألمانيا الأراضي. تحت وأزيز وفنلندا بـ, دأبوا أعلنت الإنذار، نفس تم.',
	'Product is unliked'=>'تم إلغاء اعجابك بالمنتج.',
	'Please login first to like this product'=>'الرجاء تسجيل الدخول أولاً لتسجيل إعجابك بالمنتج أو اضافته إلى المفضلة.',
	'Product is liked'=>'تم تسجيل الاعجاب بالمنتج.',
	'Product is disliked'=>'تم تسجيل عدم الاعجاب بالمنتج.',
	'Password has been updated successfully'=>'تم تحديث كلمة المرور بنجاح.',
	
	
	'Record not found'=>'لم يتم العثور على العنصر المطلوب!',
	'contact us title'=>'تواصل معنا',
	'Follow Us'=>'تابعنا',
		
	'Shipping addresses are not added yet'=>'لم يتم إضافة عنوان الشحن',
		
	'product is not added to your wish list'=>'لم يتم إضافة المنتج إلى المفضلة',
		
	'Search result for'=>'نتيجة البحث عن',
		
	'item found'=>'تم العثور على العنصر',
	'cancel'=>'إلغاء',
	
	'Please write notes of your order'=>'يرجى كتابة ملاحظات أي ملاحظات عن طلبك',
	'Coupon Applied'=>'تم تطبيق الخصم',
	
	//new
	'Welcome'=>'أهلا بك',
	'Welcome Guest'=>'مرحبا !',	
	'Worldwide Express Plus'=>'إكسبرس بلاس',
	'Standard'=>'عادي',
	'Worldwide Expedited'=>'شحن مستعجل (أولوية)',
	'Worldwide Express'=>'شحن سريع',
	'2nd Day Air A.M.'=>'شحن جوي بعد يومين',
	'Next Day Air Early A.M.'=>'الشحن جوي اليوم التالي في وقت مبكر.',
	'Next Day Air Saver'=>'اليوم التالي اقتصادي',
	'3 Day Select'=>'اختيار 3 أيام',
	'Ground'=>'بري',
	'2nd Day Air'=>'طيران بعد يومين',
	'Next Day Air'=>'طيران اليوم التالي',
	'My Account'=>'حسابي',
	'Info Pages'=>'صفحات المعلومات',
	'Dummy Text'=>'أبجد هوز دولور الجلوس امات، مينيابوليس الجامعية إيليت. لكنه يرغب في الماكياج هو أن أقول، كتلة دائما، وشحم التحرير تمويل. مجانا في بعض مكتب السهام الدعاية. لوضع توقعات واقعية. كرة القدم لايف عززت ولا عقوبة الرعاية الكلي. الحرارية ااا  كرة القدم انم، والجلوس امات خميرة التمويل في الهواء الطلقالاتحاد الأوروبي. تباينت المطورينسد، ولكن على الزناد لكرة القدم أي شركة طيران.',
	'Product is unliked'=>'تم إلغاء اعجابك بالمنتج.',
	'Please login first to like this product'=>'الرجاء تسجيل الدخول أولاً لتسجيل إعجابك بالمنتج أو اضافته إلى المفضلة.',
	'Product is liked'=>'تم تسجيل الاعجاب بالمنتج.',
	'Product is disliked'=>'تم تسجيل عدم الاعجاب بالمنتج.',
	'Password has been updated successfully'=>'تم تحديث كلمة المرور بنجاح.',
	'Empty Site Name'=>'إسم الموقع فارغ',
	'Instamojo Payment'=>'إنستا موجو',
	'External Link'=>'رابط خارجي',
	'Flash Sale'=>'التخفيضات',
	'cancel order'=>'الغاء الطلب',
	'cancelled'=>'تم الإلغاء',
	'Are you sure you want to cancel this order?'=>'هل أنت متأكد من أنك تريد إلغاء هذا الطلب؟',
	'Flash Sale'=>'التخفيضات',
	'UP COMMING'=>'قريباً',
	'Min Order Limit:'=>'الحد الأدنى للطلب:',
	'Return'=>'إرجاع',
	'Are you sure you want to return this order?'=>'هل أنت متأكد من أنك تريد إرجاع هذا الطلب؟',
	'return order'=>'إرجاع الطلب',
	'Avail free shpping on'=>'الشحن المجاني متوفر',
	
	
	//vendors
		'Sell with Us'=>'ابدأ البيــــع ',
		'New Vendors'=>'هل تريد بيع منتجات على موقعنا ؟',
    'login page text for vendor'=>'
ابدأ البيع الآن حيث يتسوق الملايين من المشترين يومياً
أنت على بعد خطوات قليلة لتصبح بائعاً على متجر رفوف',
'Registered vendors'=> ' البائعين المسجلين',
'Upload company logo'=> 'رفع شعار الشركة',
    'company name'=>'اسم الشركة',
    'employee name'=>'ممثل الشركة',
    'registration_number'=>'رقم قيد المنشأة',
    'qid_number'=>'الرقم القومى',
    'commercial_number'=>'رقم السجل التجارى',
    'company phone'=>'تليفون',
    'company fax'=>'فاكس',
    'company address'=>'العنوان',
    'company download contract'=>'تحميل العقد ورفعه بعد توقيعه',
    'company download prodducts sample'=>'تحميل نموذج ملء المنتجات', 
    'seller'=>'البائع',
    'nosales'=>'عدد المبيعات',
    'sellerRate'=>' تقييم البائع',
    'noproducts'=>'عدد المنتجات',
    'download_spec_file'=>'تحميل ملف الخصائص',
    'PDF'=>'PDF',
    'All Vendors'=>'البائعين',
    'companies' => 'الموردين',
	'deals' => 'التخفيضات',
	'categories' => 'التصنيفات',
	'About' => 'عن',
	'featured' => 'منتجات مميزة'
	
];
